package group_1_tower_defense.game_entities;

import group_1_tower_defense.Audio;
import group_1_tower_defense.Game;
import group_1_tower_defense.displayable_entities.Icons;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maxime Thibaut
 *
 * The Tower class will be it's own Factory.
 * It means that the constructors of Tower will be private and you will have to use a public static method to instantiate a new one.
 *
 */
/**
 * @author Alastra
 *
 */
public class Tower extends MapEntityAbstract
{
/*
 * FACTORY PATTERN
 */
	/**
	 * private default constructor (factory pattern)
	 */
	private Tower()
	{
	}

	/**
	 * private copy constructor (factory pattern)
	 */
	private Tower(Tower aTower)
	{
		visualID			= aTower.visualID;
		type				= aTower.type;
		setLevelup(aTower.getLevelup());
		level				= aTower.level;
		setProjectileID(aTower.getProjectileID());
		price				= aTower.price;
		reload				= aTower.reload;
		timeLeftBeforeFiring	= aTower.timeLeftBeforeFiring;
		range				= aTower.range;
		setName(aTower.getName());
	}
	
	/**
	 * array of types of towers
	 */
	private static	ArrayList<Tower> towerTypes;
	
	/**
	 * array of buildable towers' id
	 */
	private static	ArrayList<Tower> buildableTowers;

	/**
	 * add a new pre-defined object
	 * @return id of the new type of tower
	 */
	public static int	newTypeOfTower(int newVisualID, String newName, int newLevelup, int newProjectileID, int newPrice, int newReloadTime, int newRange,
										String normalIcon, String selectedIcon)
	{
		// allocate towerTypes
		if (towerTypes == null)
			towerTypes = new ArrayList<Tower>();
		if (buildableTowers == null)
			buildableTowers = new ArrayList<Tower>();
		// creating and adding the new Tower
		Tower	newTower = new Tower();
		newTower.visualID = newVisualID;
		newTower.type = towerTypes.size();
		newTower.setLevelup(newLevelup);
		newTower.level = 0;
		newTower.setProjectileID(newProjectileID);
		newTower.price = newPrice;
		newTower.reload = newReloadTime;
		newTower.timeLeftBeforeFiring = 0;
		newTower.range = newRange;
		newTower.icons = new Icons(normalIcon, selectedIcon, newTower.type);
		newTower.setName(newName);
		towerTypes.add(newTower.type, newTower);
		//updating buildableTowers list
		updateBuildableTowersList();
		// return new Tower id
		return newTower.type;
	}
	
	/**
	 * add a new pre-defined object
	 */
	public static Tower	getNewTower(int towerID)
	{
		if (0 <= towerID && towerID < towerTypes.size())
		{
			return new Tower(towerTypes.get(towerID));
		}
		else
		{
			System.err.println("Error: trying to instantiate a non existant type of tower.");
			return null;
		}
	}

	private static void	updateBuildableTowersList()
	{
		ArrayList<Tower>	nonBuildableTowers = new ArrayList<Tower>();
		buildableTowers.clear();
		for (Tower aTower : towerTypes)
		{
			if (aTower.getLevelup() >= 0 && aTower.getLevelup() < towerTypes.size())
				nonBuildableTowers.add(towerTypes.get(aTower.getLevelup()));
		}
		for (Tower aTower : towerTypes)
		{
			if (nonBuildableTowers.contains(aTower) == false)
				buildableTowers.add(aTower);
		}
	}
	
/*
 * VARIABLES
 */
	/**
	 * type of the object (id)
	 */
	protected int	type;
	
	/**
	 * name of the object (id)
	 */
	private String name;
	
	/**
	 * projectile id (use this id when calling Projectile factory method)
	 */
	private int	projectileID;
	
	/**
	 * price of the tower
	 */
	protected int	price;
	
	/**
	 * id of the upper level tower (level + 1)
	 */
	private int	levelup;
	
	/**
	 * current level of the tower
	 */
	protected int	level;
	
	/**
	 * reload speed (time in ms)
	 */
	protected long	reload;
	
	/**
	 * time left before the tower is ready to fire again (timestamp in ms)
	 */
	protected long	timeLeftBeforeFiring;
	
	/**
	 * range of the tower (radius in number of square on the map)
	 * ex: range = 3 means that the tower can fire a most at 3 cells around itself
	 */
	protected int	range;
	
	/**
	 * actual target (ref to a Enemy or null if no target)
	 */
	protected Enemy	target;
	
	/**
	 * Icons associated with the tower for the menu
	 */
	private Icons icons;

/*
 * SETTER AND GETTER
 */
	/**
	 * @return the towerTypes
	 */
	public static ArrayList<Tower> getTowerTypes() {
		return towerTypes;
	}
	
	/**
	 * @return the buildableTowers
	 */
	public static ArrayList<Tower> getBuildableTowers() {
		return buildableTowers;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @return the reload
	 */
	public long getReload() {
		return reload;
	}

	/**
	 * @return the timeLeftBeforeFiring
	 */
	public long getTimeLeft() {
		return timeLeftBeforeFiring;
	}

	/**
	 * @return the range
	 */
	public int getRange() {
		return range;
	}

	/**
	 * @return the target
	 */
	public Enemy getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(Enemy target) {
		this.target = target;
	}

	/**
	 * @return the icons
	 */
	public Icons getIcons()
	{
		return icons;
	}

	/**
	 * @param icons the icons to set
	 */
	public void setIcons(Icons icons)
	{
		this.icons = icons;
	}

	/**
	 * @return the projectileID
	 */
	public int getProjectileID()
	{
		return projectileID;
	}

	/**
	 * @param projectileID the projectileID to set
	 */
	public void setProjectileID(int projectileID)
	{
		this.projectileID = projectileID;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

/*
 * METHODS
 */
	/**
	 * @return a projectile to the target OR null (no target OR tower not ready)
	 */
	public Projectile	fire()
	{
		Projectile	newProjectile = null;
		long		currentTime = System.currentTimeMillis();
		if (target != null)
		{
			// set the orientation of the tower to look at the targeted Enemy
			float		lookX = target.getX() - x;
			float		lookY = target.getY() - y;
			float		lookNorme = (float)Math.sqrt(lookX * lookX + lookY * lookY);
			lookX /= lookNorme;
			lookY /= lookNorme;
			// look vector compared to the unit vector u(1, 0) using dot product
			// the dot product is equal to moveX in fact and updated lookNorm is 1
			orientation = (float)Math.acos(lookX) * lookY/Math.abs(lookY); // FIXME not sure that '* lookY/Math.abs(lookY)' ( *signOfYvec ) is the good calc

			if (timeLeftBeforeFiring <= currentTime)
			{
				// throw a new Projectile
				newProjectile = Projectile.getNewProjectile(getProjectileID());
				newProjectile.setX(x);
				newProjectile.setY(y);
				newProjectile.setOrientation(orientation);
				newProjectile.setTarget(target);
				Game.addProjectile(newProjectile);
				
				//audio for tower  firing
				new Audio("assets/fire_pulse4.wav").start();
				// set the time lapse before the next available fire
				timeLeftBeforeFiring = currentTime + reload;
			}
		}
		return newProjectile;
	}
	
	/**
	 * @param anEnemy is the enemy to check
	 * @return true if anEnemy is a valid target (in range)
	 */
	public boolean		isValidTarget(Enemy anEnemy)
	{
		float vecX = anEnemy.getX() - x;
		float vecY = anEnemy.getY() - y;
		float vecNorm = vecX * vecX + vecY * vecY;
		if (vecNorm <= range * range)
			return true;
		else
			return false;
	}
	
	/**
	 * @return true is the target is still valid (in range)
	 */
	public boolean		revalidateTarget()
	{
		if (target == null || isValidTarget(target) == false)
		{
			target = null;
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * try to find and assign a valid target to the tower if it has not already
	 * @param enemies the List of Enemy to check
	 * @return true if a new target has been set, false else
	 */
	public boolean		findNewTarget(List<Enemy> enemies)
	{
		if (getTarget() == null) // only one target by tower
		{
			for (Enemy anEnemy : enemies)
			{
				if (isValidTarget(anEnemy))
				{
					setTarget(anEnemy);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return the require price to pay to increase the level of the tower, -1 if the tower has no evolution
	 */
	public int		getIncreaseLevelPrice()
	{
		if (getLevelup() >= 0)
			return towerTypes.get(getLevelup()).price;
		return -1;
	}
	
	/**
	 * should not be called directly. Use the Game method to do it instead !
	 * increase the level of the tower by 1 (ONE UP !)
	 * @return true if the Tower has been increased, false else
	 */
	public boolean	increaseLevel()
	{
		if (getLevelup() >= 0)
		{
			Tower	increasedTower = towerTypes.get(getLevelup());
			visualID			= increasedTower.visualID;
			name				= increasedTower.name;
			type				= increasedTower.type;
			setLevelup(increasedTower.getLevelup());
			level++;
			setProjectileID(increasedTower.getProjectileID());
			price				+= increasedTower.price;
			reload				= increasedTower.reload;
			//timeLeftBeforeFiring	= increasedTower.timeLeftBeforeFiring;
			range				= increasedTower.range;
			Game.getDisplayable().mp.displayTowerVariables(increasedTower);// FIXME Tower should not use the Game class to avoid strong dependencies between classes. Can call it in Game when this method return true
			return true;
		}
		return false;
	}

	public int getLevelup()
	{
		return levelup;
	}

	public void setLevelup(int levelup)
	{
		this.levelup = levelup;
	}
}
