package group_1_tower_defense.game_entities;

/**
 * @author Guillaume Lebedel
 * 
 * MapElement represent one square of the grid.
 * 
 * So we find a (X & Y) position relative to the grid.
 * FullSquare is the define for a square which cannot be used as a possible node for the pathFinder
 * EmptySquare is the define for one which can be used
 */
public class MapElement
{
	public static final int EMPTY_SQUARE	= 0;
	public static final int START_POINT		= 1;
	public static final int END_POINT		= 2;
	public static final int WALL			= 3;
	public static final int TOWER			= 4;
	
	public static final int CONSTRUCTABLE_LIMIT = 2;

	private int x;
	private int y;
	private int type = EMPTY_SQUARE;

	public MapElement(MapElement element)
	{
		this.x = element.getX();
		this.y = element.getY();
		this.type = element.getType();
	}
	public MapElement(int x, int y, int type)
	{
		this.x = x;
		this.y = y;
		this.type = type;
	}
	
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public int getX()
	{
		return x;
	}
	public void setX(int x)
	{
		this.x = x;
	}
	public int getY()
	{
		return y;
	}
	public void setY(int y)
	{
		this.y = y;
	}
}
