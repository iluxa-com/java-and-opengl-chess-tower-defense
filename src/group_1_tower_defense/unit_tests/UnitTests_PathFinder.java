package group_1_tower_defense.unit_tests;

import group_1_tower_defense.Game;
import group_1_tower_defense.Parser;
import group_1_tower_defense.PathFinder;
import group_1_tower_defense.game_entities.MapElement;

public class UnitTests_PathFinder  {

	public static int WIDTH = 11;
	public static int HEIGHT = 11;
	public static String[][] map = {
		{"x","x", "x", "x", "x", "x", "o", "x", "x", "x", "x"},
		{"x","x", "o", "o", "o", "o", "o", "o", "o", "o", "x"},
		{"x","x", "o", "o", "o", "x", "x", "o", "o", "o", "x"},
		{"x","x", "o", "o", "x", "o", "o", "x", "x", "o", "x"},
		{"o","o", "o", "x", "o", "o", "x", "o", "o", "o", "x"},
		{"x","x", "o", "x", "o", "x", "o", "x", "o", "o", "o"},
		{"x","x", "o", "o", "o", "x", "o", "x", "o", "o", "o"},
		{"x","o", "o", "o", "x", "x", "o", "x", "o", "o", "o"},
		{"x","o", "o", "o", "o", "x", "o", "x", "o", "o", "o"},
		{"x","o", "o", "x", "o", "o", "o", "o", "o", "o", "x"},
		{"x","x", "x", "x", "x", "x", "o", "x", "x", "x", "x"}
	};	
	public static String[][] map2 = {
		{"x","x", "x", "x", "x", "x", "x", "x", "x", "x", "x"},
		{"x","x", "o", "o", "o", "o", "o", "o", "o", "o", "x"},
		{"x","x", "o", "o", "o", "x", "x", "o", "o", "o", "x"},
		{"x","x", "o", "o", "x", "o", "o", "x", "x", "o", "x"},
		{"o","o", "o", "o", "o", "o", "x", "o", "o", "o", "x"},
		{"x","x", "o", "x", "o", "o", "o", "x", "o", "o", "o"},
		{"x","x", "o", "x", "o", "x", "o", "x", "o", "o", "o"},
		{"x","o", "o", "o", "x", "o", "o", "x", "o", "o", "x"},
		{"x","o", "o", "o", "o", "x", "o", "x", "o", "o", "x"},
		{"x","o", "o", "x", "o", "o", "o", "o", "o", "o", "x"},
		{"x","x", "x", "x", "x", "x", "x", "x", "x", "x", "x"}
	};

	public static void setGrid(MapElement[][]grid, String[][] mmap)
	{
		for (int i =0; i < HEIGHT; i++)
			for (int j = 0; j < WIDTH; j++)
				grid[i][j] = new MapElement(j, i, (mmap[i][j] == "x" ? MapElement.WALL : MapElement.EMPTY_SQUARE));
	}

private static String pathFile = "config_files/settings.xml";
	public static void main(String[] args) 
	{
		/*MapElement[][] grid = new MapElement[HEIGHT][WIDTH];
		MapElement[][] grid2 = new MapElement[HEIGHT][WIDTH];
		setGrid(grid, map); // transform an ascii map into a two dimensional MapElement array
		setGrid(grid2, map2);
		Map myMap = new Map(WIDTH, HEIGHT, grid); // create a map from the 2d array of mapElement*/
		
		Parser xmlParse = new Parser();
		xmlParse.loadFile(pathFile);
		System.out.print("=> start life :"+Game.getLifePoints()+"\n");
		System.out.print("=> start money :"+Game.getMoneyPoints()+"\n");
		PathFinder test = new PathFinder();
		test.init(Game.getMap()); // Instantiate the pathFinding class with the end of the path wished as the first parameter and the map of  the game as the second
		test.printAllPath(); // print and calculate all path
		//System.out.print("ADD END POINTS\n");
		//test.addEndPoint(grid[5][8]);
		//test.addEndPoint(grid[4][8]);
		//test.addEndPoint(grid[2][2]);
		//test.printAllPath(); // print and calculate all path
		//myMap = new Map(MainPathFinder.WIDTH, MainPathFinder.HEIGHT, grid2); 
		//test.modifyGameMap(myMap);// change the map of the pathFinding class
		//test.printAllPath(); // print and calculate all the path for this new map
		
		MapElement temp;
		temp = test.getNextNode(Game.getMap().getGrid()[4][5]);
		System.out.print(temp.getY() +":" + temp.getX() + "\n");
		test.printPath(test.getFlyingWholePath(Game.getMap().getGrid()[1][1]));
		System.out.print(test.blockPath(Game.getMap().getGrid()[2][5], Game.getMap().getGrid()[3][6]));
		return ;
	}
}
