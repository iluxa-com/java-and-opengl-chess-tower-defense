package group_1_tower_defense.displayable_entities;

import com.jogamp.common.nio.Buffers;

import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
//import javax.media.opengl.GL;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;


public class OBJloader {
	/**
	 * Reads in OBJ format files and sets the files for rendering with OpenGL
	 */
    private String OBJModelPath; // The path to the Model File
    private String OBJImagePath; // The path to the image file for the model
    private ArrayList vData = new ArrayList(); // List of Vertex Coordinates
    private ArrayList vtData = new ArrayList(); // List of Texture Coordinates
    private ArrayList vnData = new ArrayList(); // List of Normal Coordinates
    private ArrayList fv = new ArrayList(); // Face Vertex Indices
    private ArrayList ft = new ArrayList(); // Face Texture Indices
    private ArrayList fn = new ArrayList(); // Face Normal Indices
    private FloatBuffer modeldata; // Buffer which will contain vertice data
    private int FaceFormat; // Format of the Faces Triangles or Quads
    private int FaceMultiplier; // Number of possible coordinates per face
    private int PolyCount = 0; // The Models Polygon Count
    
    BufferedImage bufferedImage = null;
    int w;
    int h;
    File  file = null;
    ByteBuffer bb = null;
    
    
    /**
     * OBJ loader constructor, takes in a string for a Modelpath and a string for Imagepath
     * both are set to be processed by LoadOBJModel
     * @param Modelpath
     * @param Imagepath
     */
    public OBJloader(String Modelpath, String Imagepath) {
        OBJModelPath = Modelpath;
        OBJImagePath = Imagepath;
        LoadOBJModel(OBJModelPath, OBJImagePath);
        SetFaceRenderType();
        
    }
    /**
     * Loads OBJmodel data from string directory into appropriate ArrayLists, line by line
     * Loads  Image data from string directory into buffers to be applied to the finished model.
     * @param ModelPath
     * @param ImagePath
     */
    private void LoadOBJModel(String ModelPath, String ImagePath) 
    {
        try {
            // Open a file handle and read the model data and image data
            BufferedReader br = new BufferedReader(new FileReader(ModelPath));
            file = new File(ImagePath);
            
            String  line = null;
            while((line = br.readLine()) != null) 
            {
            	// Read Any Descriptor Data in the File
                if (line.startsWith("#")) 
                { 
                    System.out.println("Description: "+line);
                } 
                
                else if (line.equals("")) 
                {
                	//Ignore whitespace data
                } 
                
                // Read in Vertex Data
                else if (line.startsWith("v ")) 
                { 
                    vData.add(ProcessData(line));
                } 
                
                // Read Texture Coordinates
                else if (line.startsWith("vt ")) 
                { 
                    vtData.add(ProcessData(line));
                } 
                
                // Read Normal Coordinates
                else if (line.startsWith("vn ")) 
                {
                    vnData.add(ProcessData(line));
                } 
                
                // Read Face Data
                else if (line.startsWith("f ")) 
                { 
                    ProcessfData(line);
                }
            }
            
        	bufferedImage = ImageIO.read(file);
			w = bufferedImage.getWidth();
			h = bufferedImage.getHeight();
			
			WritableRaster raster = Raster.createInterleavedRaster (DataBuffer.TYPE_BYTE, w, h, 4, null);
			ComponentColorModel colorModel = new ComponentColorModel (ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] {8,8,8,8}, true, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
			BufferedImage dukeImg = new BufferedImage (colorModel, raster, false, null);
	 
			Graphics2D g = dukeImg.createGraphics();
			g.drawImage(bufferedImage, null, null);
			DataBufferByte dukeBuf = (DataBufferByte)raster.getDataBuffer();
			byte[] dukeRGBA = dukeBuf.getData();
			bb = ByteBuffer.wrap(dukeRGBA);
			
			bb.position(0);
			bb.mark();
		
            br.close();
     
            
        } catch(IOException e) {
            System.out.println("Failed to find or read OBJ: " + ModelPath);
            System.err.println(e);
        }
    }
    
   /**
    * Processes the data read in by a string of information and splits the data on white space
    * @param read
    * @return ProcessFloatData
    */
    private float[] ProcessData(String read) 
    {
        final String s[] = read.split("\\s+");
        return (ProcessFloatData(s)); //returns an array of processed float data
    }
    
   /**
    * Parse data as floats to be used for OpenGL 
    * @param sdata
    * @return
    */
    private float[] ProcessFloatData(String sdata[]) 
    {
        float data[] = new float[sdata.length-1];
        
        for (int loop=0; loop < data.length; loop++)
        {
            data[loop] = Float.parseFloat(sdata[loop+1]);
        }
       
        
        return data; // return an array of floats
   
    }
    
    
    /**
     * Reads in the face data of an OBJ model
     * Face data consists of the locations of the vector data read in
     * splits on white space and reads on forward slash
     * a 0 is inserted for any data missing.
     * @param fread
     */
    private void ProcessfData(String fread) 
    {
        PolyCount++;
        String s[] = fread.split("\\s+");
        if (fread.contains("//")) 
        { // Pattern is present if obj has only v and vn in face data
            
        	for (int loop=1; loop < s.length; loop++) 
            {
                s[loop] = s[loop].replaceAll("//","/0/"); 
            }
     
        }
        ProcessfIntData(s); // Pass in face data
    }
    
    /**
     * Processes all integer data read in by the OBJ file format
     * splits the data up into 3 groups to choose for processing
     *  - vertex data
     *  - vertex texture data
     *  - vertex normal data
     * @param sdata
     */
    private void ProcessfIntData(String sdata[]) 
    {
        int vdata[] = new int[sdata.length-1];
        int vtdata[] = new int[sdata.length-1];
        int vndata[] = new int[sdata.length-1];
        for (int loop = 1; loop < sdata.length; loop++) 
        {
            String s = sdata[loop];
            String[] temp = s.split("/");
            vdata[loop-1] = Integer.valueOf(temp[0]); 
            if (temp.length > 1) 
            { // v and vt data
                vtdata[loop-1] = Integer.valueOf(temp[1]); 
            } 
            
            else 
            {
                vtdata[loop-1] = 0; 
            }
            
            if (temp.length > 2) 
            { // v, vt, and vn data
                vndata[loop-1] = Integer.valueOf(temp[2]);
            } 
            
            else 
            {
                vndata[loop-1] = 0; 
            }
        }
        fv.add(vdata);
        ft.add(vtdata);
        fn.add(vndata);
    }
    
    /**
     * OBJ models as two major types
     *  - Triangulated Data
     *  - Quadratic Data
     *  - if it is not Triangulated or Quadratic, it is unknown.
     */
    private void SetFaceRenderType() 
    {
        final int temp [] = (int[]) fv.get(0);
       
        if ( temp.length == 3)
        { 
            FaceFormat = GL2.GL_TRIANGLES; 
            FaceMultiplier = 3;
        }
       
        else if (temp.length == 4) 
        {
            FaceFormat = GL2.GL_QUADS; 
            FaceMultiplier = 4;
        }
        
        else 
        {
        	System.out.println("UNKNOWN TYPE");
            FaceFormat = 0; 
            System.exit(1);
        }
    }
    
    /**
     * Constructs the array necessary to be read in by the glDrawArrays method
     * Calls on construction functions for V, TV, NV and TNV
     * @param gl
     */
    private void ConstructInterleavedArray(GL2 gl) 
    {
        final int tv[] = (int[]) fv.get(0);
        final int tt[] = (int[]) ft.get(0);
        final int tn[] = (int[]) fn.get(0);
        // If a value of zero is found that it tells us we don't have that type of data
        if ((tv[0] != 0) && (tt[0] != 0) && (tn[0] != 0)) 
        {
            ConstructTNV(); //We have Vertex, 2D Texture, and Normal Data
            gl.glInterleavedArrays(GL2.GL_T2F_N3F_V3F, 0, modeldata);
        } 
        
        else if ((tv[0] != 0) && (tt[0] != 0) && (tn[0] == 0))
        {
            ConstructTV(); //We have just vertex and 2D texture Data
            gl.glInterleavedArrays(GL2.GL_T2F_V3F, 0, modeldata);
        } 
        
        else if ((tv[0] != 0) && (tt[0] == 0) && (tn[0] != 0)) 
        {
            ConstructNV(); //We have just vertex and normal Data
            gl.glInterleavedArrays(GL2.GL_N3F_V3F, 0, modeldata);
        } 
        
        else if ((tv[0] != 0) && (tt[0] == 0) && (tn[0] == 0)) 
        {
            ConstructV();
            gl.glInterleavedArrays(GL2.GL_V3F, 0, modeldata);
        }
    }
    
    /**
     * Used specifically for creating Texture Normals for proper texture assignment.
     */
    private void ConstructTNV() {
        int[] v, t, n;
        float tcoords[] = new float[2]; 
        float coords[] = new float[3];        
        int fbSize= PolyCount*(FaceMultiplier*8); // 3 verts Per Poly, 2 vertex textures Per Poly, 3 vertex normals Per Poly
        modeldata = Buffers.newDirectFloatBuffer(fbSize);
        modeldata.position(0);     
        for (int oloop=0; oloop < fv.size(); oloop++) 
        {
            v = (int[])(fv.get(oloop));
            t = (int[])(ft.get(oloop));
            n = (int[])(fn.get(oloop));
            for (int iloop=0; iloop < v.length; iloop++) 
            {
                for (int tloop=0; tloop < tcoords.length; tloop++) 
                    tcoords[tloop] = ((float[])vtData.get(t[iloop] - 1))[tloop];
                modeldata.put(tcoords);
                // Fill in the normal coordinate data
                for (int vnloop=0; vnloop < coords.length; vnloop++)
                    coords[vnloop] = ((float[])vnData.get(n[iloop] - 1))[vnloop];
                modeldata.put(coords);
                // Fill in the vertex coordinate data
                for (int vloop=0; vloop < coords.length; vloop++)
                    coords[vloop] = ((float[])vData.get(v[iloop] - 1))[vloop];
                modeldata.put(coords);
            }
        }
        modeldata.position(0); 
    }
    
    /**
     * Constructs the Texture Vertices used in ONLY an X and Y plane (2D Textures).
     */
    private void ConstructTV() 
    {
        int[] v, t;
        float tcoords[] = new float[2]; 
        float coords[] = new float[3];
        int fbSize= PolyCount*(FaceMultiplier*5); // 3 verts Per Poly, 2 vertex textures Per Poly  
        modeldata = Buffers.newDirectFloatBuffer(fbSize);
        modeldata.position(0);
        for (int oloop=0; oloop < fv.size(); oloop++) 
        {
            v = (int[])(fv.get(oloop));
            t = (int[])(ft.get(oloop));
            for (int iloop=0; iloop < v.length; iloop++) 
            {
                // Fill in the texture coordinate data
                for (int tloop=0; tloop < tcoords.length; tloop++) //Only T2F is supported in InterLeavedArrays!!
                    tcoords[tloop] = ((float[])vtData.get(t[iloop] - 1))[tloop];                
                modeldata.put(tcoords);
                // Fill in the vertex coordinate data
                for (int vloop=0; vloop < coords.length; vloop++)
                    coords[vloop] = ((float[])vData.get(v[iloop] - 1))[vloop];
                modeldata.put(coords);
            }
        }
        modeldata.position(0);
    }
    
    /**
     * Constructs the Vertex Normals used for lighting and texturing.
     */
    private void ConstructNV() {
        int[] v, n;
        float coords[] = new float[3];
        int fbSize= PolyCount*(FaceMultiplier*6); // 3 verts Per Poly, 3 vertex normals Per Poly
        modeldata = Buffers.newDirectFloatBuffer(fbSize);
        modeldata.position(0);
        for (int oloop=0; oloop < fv.size(); oloop++) 
        {
            v = (int[])(fv.get(oloop));
            n = (int[])(fn.get(oloop));
            for (int iloop=0; iloop < v.length; iloop++) 
            {
                // Fill in the normal coordinate data
                for (int vnloop=0; vnloop < coords.length; vnloop++)
                    coords[vnloop] = ((float[])vnData.get(n[iloop] - 1))[vnloop];
                modeldata.put(coords);
                // Fill in the vertex coordinate data
                for (int vloop=0; vloop < coords.length; vloop++)
                    coords[vloop] = ((float[])vData.get(v[iloop] - 1))[vloop];
                modeldata.put(coords);
            }
        }
        modeldata.position(0);
    }
    
    /**
     * Constructs the Vector array for rendering the OBJ model.
     */
    private void ConstructV() {
        int[] v;
        float coords[] = new float[3];
        int fbSize= PolyCount*(FaceMultiplier*3); // 3 verts Per Poly
        modeldata = Buffers.newDirectFloatBuffer(fbSize);
        modeldata.position(0);
        for (int oloop=0; oloop < fv.size(); oloop++) 
        {
            v = (int[])(fv.get(oloop));
            for (int iloop=0; iloop < v.length; iloop++) 
            {
                // Fill in the vertex coordinate data
                for (int vloop=0; vloop < coords.length; vloop++)
                    coords[vloop] = ((float[])vData.get(v[iloop] - 1))[vloop];
                modeldata.put(coords);
            }
        }
        modeldata.position(0);
    }
       
    /**
     * Draws the final OBJ model by constructing and parsing through the vector arrays
     * and assigns the texture to the given model.
     * A rotation function is used so that the loaded objects may 'look' at each other.
     * @param gl
     * @param rotate
     */
    public void DrawModel(GL2 gl, float rotate) 
    {
    	gl.glPushMatrix();
    	
    	//this glRotate function calls on the first parameter for degree and 1f on the y
    	gl.glRotatef(rotate, 0f, 1f, 0f);
    		
        ConstructInterleavedArray(gl);
        
		gl.glBindTexture(GL2.GL_TEXTURE_2D, 13);
		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);  //disable this and uses environment lighting
		gl.glTexImage2D (GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, w, h, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, bb);
		
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glBindTexture (GL2.GL_TEXTURE_2D, 13);
        
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glCullFace(GL2.GL_BACK); 
        gl.glLineWidth(2); //for GL_LINE command, increases thickness of line
        gl.glPolygonMode(GL2.GL_FRONT,GL2.GL_SMOOTH); //can use GL_LINE as well
        gl.glShadeModel(GL2.GL_SMOOTH);
        
        gl.glDrawArrays(FaceFormat, 0, PolyCount*FaceMultiplier); 
      
        gl.glEnd();
        gl.glDisable(GL2.GL_CULL_FACE);
       gl.glPopMatrix();

        
    }
    
}