***TEAM 1 - 3D OpenGL Tower Defense***
The Tower Defense game was compiled using JOGL for 32-bit Windows based platforms.  However if you are using a different operating system (be it 32-bit or 64-bit) We have included the necessary .jar files to import into Eclipse and run the Tower Defense game.



Instructions on applying your own JOGL OpenGL functionality:
* Find the corret Gluegen / Jogl 7z archive for your operating system and architecture, the Gluegen / Jogl files are included in this repository, it is the file labeled JOGL.zip

* Remove the existing 32-bit JOGL build-path files under 'Referenced Libraries' from Eclipse

* remove any files in the 'lib' directory in the project in Eclipse

* In the folder 'lib', drag and drop the files from your selected Gluegen / Jogl archive:

- In the Gluegen archive:
  /jar/gluegen-rt.jar
  /jar/gluegen-rt-native-xxxxxxxxxx  (with 'xxxxx...' being the name of the OS and architecture)
- In the Jogl archive:
  /jar/jogl.all.jar
  /jar/jogl-all-natives-xxxxxxxxxx (with 'xxxxx...' being the name of the OS and architecture)

* In Eclipse, select all the Gluegen / Jogl jar files that you have selected, right click (or control-click for Apple) and select 'Add these files to build path'

* Congratulations! You are ready to play Tower Defense in 3D! As well as build other amazing projects using OpenGL functionality!
